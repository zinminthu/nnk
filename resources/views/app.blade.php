<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Doucment</title>
    <!-- <link rel="stylesheet" href="/app/bootstrap/app.php"> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      @yield('content')
    </div>
    @yield('footer')
  </body>
</html>

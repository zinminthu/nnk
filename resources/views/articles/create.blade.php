@extends('app')

@section('content')

  <h1>Write a New Articles</h1>
  <hr>
  <form class="form-group" action="{{URL::to('/articles')}}" method="post">
    <div class="form-group">
      <label for="">Title</label>
      <input type="text" name="title " value="" class="form-control">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
    </div>
    <div class="form-group">
      <label for="">Body</label>
      <textarea name="body" rows="8" cols="80" class="form-control"></textarea>
    </div>
    <div class="form-group">
      <input type="submit" name="" value="Insert" class="btn btn-primary">

    </div>

  </form>
@stop

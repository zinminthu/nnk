<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function about(){
      //$name='ko <span style="color:red;">zin</span>';
      // return view('pages.about')->with([
      //   'first'=>'ko',
      //   'last'=>'zin'
      // ]);
      $people=['kozin','NayToe','MyintMyat'];
      return view('pages.about',compact('people'));
    }

    public function contact(){
      return view('pages.contact');
    }
}

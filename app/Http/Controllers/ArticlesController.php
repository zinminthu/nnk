<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{
    //
    public function index(){
      // return "get all articles";
      $articles=Article::all();
      return view('articles.index',compact('articles'));
      // return $articles;
    }

    public function show ($id){
      // return $id;
      $article=Article::find($id);
      // return $article;
      return view('articles.show',compact('article'));
    }


    public function create(){
      return view('articles.create');

    }

    public function store(Request $request){
      $input=$request->input('title');
      echo $input;
      // return "hello";
    }
}
